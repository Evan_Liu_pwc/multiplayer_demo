﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SyncPlayer : NetworkBehaviour
{

    public GameObject player;
    public GameObject body;

    // Use this for initialization
    void Start () {
        if (!isLocalPlayer)
        {
            player.SetActive(false);
        }
    }
	
	// Update is called once per frame
	void Update () {

    }
}
