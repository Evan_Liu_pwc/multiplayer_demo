﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Prototype.NetworkLobby
{
    public class AsServer : MonoBehaviour
    {

        public LobbyManager lobbyManager;

        // Use this for initialization
        void Start()
        {
            lobbyManager.StartHost();
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
