﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Prototype.NetworkLobby
{
    public class AsClient : MonoBehaviour
    {

        public LobbyManager lobbyManager;

        // Use this for initialization
        void Start()
        {
            lobbyManager.networkAddress = "192.168.0.50";
            lobbyManager.networkPort = 3306;
            lobbyManager.StartClient();
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
