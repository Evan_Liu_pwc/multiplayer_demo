﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerSync : MonoBehaviour {

    public GameObject player_head;
    public GameObject player_lefthand;
    public GameObject player_righthand;

    public GameObject head;
    public GameObject lefthand;
    public GameObject righthand;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //Sync Position
        head.transform.position = player_head.transform.position;
        lefthand.transform.position = player_lefthand.transform.position;
        righthand.transform.position = player_righthand.transform.position;

        //Sync Rotation
        head.transform.rotation = player_head.transform.rotation;
        lefthand.transform.rotation = player_lefthand.transform.rotation;
        righthand.transform.rotation = player_righthand.transform.rotation;
    }
}
