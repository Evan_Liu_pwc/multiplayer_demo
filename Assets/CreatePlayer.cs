﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CreatePlayer : MonoBehaviour {
    public GameObject player;
    // Use this for initialization
    void Start () {
        Instantiate(player, new Vector3(0, 0, 0), Quaternion.identity);
        NetworkServer.Spawn(player);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
